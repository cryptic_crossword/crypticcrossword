Cryptic Crossword

given a cryptic crossword with black and white boxes
number these boxes row-wise
input is a list of strings representing each row
output needs to be a list of tuples with integer in the formant (row, col, num)